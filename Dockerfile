FROM openjdk:11
ADD target/*.jar docker-cicd.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar" , "docker-cicd.jar"]